#! /bin/sh
# postinst script for gitlab
# copied from postinst script for hplip
# $Id: hplip.postinst,v 1.1 2005/10/15 21:39:04 hmh Exp $
#
# see: dh_installdeb(1)

set -e

# Show debconf questions
. /usr/share/debconf/confmodule

gitlab_common_defaults=/usr/lib/gitlab-common/gitlab-common.defaults
gitlab_common_defaults_copy=/var/lib/gitlab-common/gitlab-common.defaults

# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package
#
# quoting from the policy:
#     Any necessary prompting should almost always be confined to the
#     post-installation script, and should be protected with a conditional
#     so that unnecessary prompting doesn't happen if a package's
#     installation fails and the `postinst' is called with `abort-upgrade',
#     `abort-remove' or `abort-deconfigure'.


case "$1" in
  configure)
    # Read default values
    . ${gitlab_common_defaults}

    # Copy defaults for use with postrm
    cp ${gitlab_common_defaults} ${gitlab_common_defaults_copy}

    # Read gitlab_user from debconf db
    db_get gitlab-common/user
    gitlab_user=$RET >/dev/null

    # Create gitlab user
    . /usr/lib/gitlab-common/scripts/adduser.sh

    # Keep config file and debconf db in sync
    touch ${gitlab_common_conf_private} 
    test -z "$gitlab_user" || grep -Eq '^ *gitlab_user=' ${gitlab_common_conf_private} || \
        echo "gitlab_user=" >> ${gitlab_common_conf_private}
    sed -e "s/^ *gitlab_user=.*/gitlab_user=\"$gitlab_user\"/" \
            < ${gitlab_common_conf_private} > ${gitlab_common_conf_private}.tmp
    mv -f ${gitlab_common_conf_private}.tmp ${gitlab_common_conf_private}

    echo "Registering ${gitlab_common_conf} via ucf"
    ucf --debconf-ok --three-way ${gitlab_common_conf_private} ${gitlab_common_conf}
    ucfr gitlab-common ${gitlab_common_conf}

    # Manage tmpfiles.d/gitlab-common.conf via ucf
    cp ${gitlab_common_tmpfiles_example} ${gitlab_common_tmpfiles_private}
    sed -i "s/GITLAB_USER/${gitlab_user}/" ${gitlab_common_tmpfiles_private}
    echo "Registering ${gitlab_common_tmpfiles} via ucf"
    ucf --debconf-ok --three-way ${gitlab_common_tmpfiles_private} ${gitlab_common_tmpfiles}
    ucfr gitlab-common ${gitlab_common_tmpfiles}
    ;;

  triggered)
    # Already handled
    ;;

  abort-upgrade|abort-remove|abort-deconfigure)
    ;;

  *)
    echo "postinst called with unknown argument \`$1'" >&2
    exit 1
    ;;
esac

db_stop

#DEBHELPER#
