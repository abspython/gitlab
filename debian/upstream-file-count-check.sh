  LC_ALL=C.UTF-8 ls -1a |grep -vx .git |grep -vx .pc > debian/upstream-file-list.new
if ! diff -u debian/upstream-file-list debian/upstream-file-list.new
then
  echo "Upstream added or removed files"
  echo "Update 'debian/gitlab.install' and/or 'debian/ignored-file-list'"
  echo "\nNewly added files are:"
  echo "---------------------"
  comm -23 debian/upstream-file-list.new debian/upstream-file-list
  echo "---------------------\n"
  echo "Removed files are:"
  echo "---------------------"
  comm -13 debian/upstream-file-list.new debian/upstream-file-list
  echo "---------------------"
  echo "\nAfter updating the ignore and install files, rename 'debian/upstream-file-list.new' as 'debian/upstream-file-list'. If not using dpkg-buildpackage, generate 'debian/upstream-file-list.new' manually and then rename.\n"
  exit 1
fi
