We should be able to update minor versions of stable libs without breaking
gitlab Gemfile

--- a/Gemfile
+++ b/Gemfile
@@ -10,10 +10,10 @@
 # Responders respond_to and respond_with
 gem 'responders', '~> 2.0'
 
-gem 'sprockets', '~> 3.7.0'
+gem 'sprockets', '~> 3.7'
 
 # Default values for AR models
-gem 'default_value_for', '~> 3.2.0'
+gem 'default_value_for', '~> 3.2'
 
 # Supported DBs
 gem 'pg', '~> 1.1'
@@ -28,25 +28,25 @@
 gem 'doorkeeper', '~> 4.3'
 gem 'doorkeeper-openid_connect', '~> 1.5'
 gem 'omniauth', '~> 1.8'
-gem 'omniauth-auth0', '~> 2.0.0'
+gem 'omniauth-auth0', '~> 2.0'
 gem 'omniauth-azure-oauth2', '~> 0.0.9'
-gem 'omniauth-cas3', '~> 1.1.4'
-gem 'omniauth-facebook', '~> 4.0.0'
+gem 'omniauth-cas3', '~> 1.1', '>= 1.1.4'
+gem 'omniauth-facebook', '~> 4.0'
 gem 'omniauth-github', '~> 1.3'
-gem 'omniauth-gitlab', '~> 1.0.2'
+gem 'omniauth-gitlab', '~> 1.0', '>= 1.0.2'
 gem 'omniauth-google-oauth2', '~> 0.6.0'
 gem 'omniauth-kerberos', '~> 0.3.0', group: :kerberos
 gem 'omniauth-oauth2-generic', '~> 0.2.2'
 gem 'omniauth-saml', '~> 1.10'
-gem 'omniauth-shibboleth', '~> 1.3.0'
+gem 'omniauth-shibboleth', '~> 1.3'
 gem 'omniauth-twitter', '~> 1.4'
-gem 'omniauth_crowd', '~> 2.2.0'
+gem 'omniauth_crowd', '~> 2.2'
 gem 'omniauth-authentiq', '~> 0.3.3'
 gem 'omniauth_openid_connect', '~> 0.3.1'
 gem "omniauth-ultraauth", '~> 0.0.2'
-gem 'omniauth-salesforce', '~> 1.0.5'
-gem 'rack-oauth2', '~> 1.9.3'
-gem 'jwt', '~> 2.1.0'
+gem 'omniauth-salesforce', '~> 1.0', '>= 1.0.5'
+gem 'rack-oauth2', '~> 1.9', '>= 1.9.3'
+gem 'jwt', '~> 2.1'
 
 # Kerberos authentication. EE-only
 gem 'gssapi', group: :kerberos
@@ -57,41 +57,41 @@
 gem 'invisible_captcha', '~> 0.12.1'
 
 # Two-factor authentication
-gem 'devise-two-factor', '~> 3.0.0'
+gem 'devise-two-factor', '~> 3.0'
 gem 'rqrcode-rails3', '~> 0.1.7'
-gem 'attr_encrypted', '~> 3.1.0'
+gem 'attr_encrypted', '~> 3.1'
 gem 'u2f', '~> 0.2.1'
 
 # GitLab Pages
-gem 'validates_hostname', '~> 1.0.6'
-gem 'rubyzip', '~> 1.2.2', require: 'zip'
+gem 'validates_hostname', '~> 1.0', '>= 1.0.6'
+gem 'rubyzip', '~> 1.2', '>= 1.2.2', require: 'zip'
 # GitLab Pages letsencrypt support
-gem 'acme-client', '~> 2.0.2'
+gem 'acme-client', '~> 2.0', '>= 2.0.2'
 
 # Browser detection
 gem 'browser', '~> 2.5'
 
 # GPG
-gem 'gpgme', '~> 2.0.18'
+gem 'gpgme', '~> 2.0', '>= 2.0.18'
 
 # LDAP Auth
 # GitLab fork with several improvements to original library. For full list of changes
 # see https://github.com/intridea/omniauth-ldap/compare/master...gitlabhq:master
-gem 'gitlab_omniauth-ldap', '~> 2.1.1', require: 'omniauth-ldap'
+gem 'gitlab_omniauth-ldap', '~> 2.1', '>= 2.1.1', require: 'omniauth-ldap'
 gem 'net-ldap'
 
 # API
-gem 'grape', '~> 1.1.0'
+gem 'grape', '~> 1.1'
 gem 'grape-entity', '~> 0.7.1'
-gem 'rack-cors', '~> 1.0.0', require: 'rack/cors'
+gem 'rack-cors', '~> 1.0', require: 'rack/cors'
 
 # GraphQL API
-gem 'graphql', '~> 1.9.11'
+gem 'graphql', '~> 1.9', '>= 1.9.11'
 # NOTE: graphiql-rails v1.5+ doesn't work: https://gitlab.com/gitlab-org/gitlab/issues/31771
 # TODO: remove app/views/graphiql/rails/editors/show.html.erb when https://github.com/rmosolgo/graphiql-rails/pull/71 is released:
 # https://gitlab.com/gitlab-org/gitlab/issues/31747
-gem 'graphiql-rails', '~> 1.4.10'
-gem 'apollo_upload_server', '~> 2.0.0.beta3'
+gem 'graphiql-rails', '~> 1.4', '>= 1.4.10'
+gem 'apollo_upload_server', '>= 2.0.0.beta3'
 gem 'graphql-docs', '~> 1.6.0', group: [:development, :test]
 
 # Disable strong_params so that Mash does not respond to :permitted?
@@ -101,7 +101,7 @@
 gem 'kaminari', '~> 1.0'
 
 # HAML
-gem 'hamlit', '~> 2.8.8'
+gem 'hamlit', '~> 2.8', '>= 2.8.8'
 
 # Files attachments
 gem 'carrierwave', '~> 1.3'
@@ -111,7 +111,7 @@
 gem 'fog-aws', '~> 3.5'
 # Locked until fog-google resolves https://github.com/fog/fog-google/issues/421.
 # Also see config/initializers/fog_core_patch.rb.
-gem 'fog-core', '= 2.1.0'
+gem 'fog-core', '= 2.1'
 gem 'fog-google', '~> 1.9'
 gem 'fog-local', '~> 0.6'
 gem 'fog-openstack', '~> 1.0'
@@ -125,46 +125,46 @@
 gem 'unf', '~> 0.1.4'
 
 # Seed data
-gem 'seed-fu', '~> 2.3.7'
+gem 'seed-fu', '~> 2.3', '>= 2.3.7'
 
 # Search
 gem 'elasticsearch-model', '~> 0.1.9'
 gem 'elasticsearch-rails', '~> 0.1.9', require: 'elasticsearch/rails/instrumentation'
-gem 'elasticsearch-api',   '5.0.3'
+gem 'elasticsearch-api',   '~> 5.0', '>= 5.0.3'
 gem 'aws-sdk'
 gem 'faraday_middleware-aws-signers-v4'
 
 # Markdown and HTML processing
 gem 'html-pipeline', '~> 2.8'
-gem 'deckar01-task_list', '2.2.0'
-gem 'gitlab-markup', '~> 1.7.0'
-gem 'github-markup', '~> 1.7.0', require: 'github/markup'
+gem 'deckar01-task_list', '2.2'
+gem 'gitlab-markup', '~> 1.7'
+gem 'github-markup', '~> 1.7', require: 'github/markup'
 gem 'commonmarker', '~> 0.17'
-gem 'RedCloth', '~> 4.3.2'
+gem 'RedCloth', '~> 4.3', '>= 4.3.2'
 gem 'rdoc', '~> 6.0'
 gem 'org-ruby', '~> 0.9.12'
 gem 'creole', '~> 0.5.0'
 gem 'wikicloth', '0.8.1'
-gem 'asciidoctor', '~> 2.0.10'
+gem 'asciidoctor', '~> 2.0', '>= 2.0.10'
 gem 'asciidoctor-include-ext', '~> 0.3.1', require: false
 gem 'asciidoctor-plantuml', '0.0.9'
-gem 'rouge', '~> 3.11.0'
+gem 'rouge', '~> 3.11'
 gem 'truncato', '~> 0.7.11'
-gem 'bootstrap_form', '~> 4.2.0'
-gem 'nokogiri', '~> 1.10.4'
+gem 'bootstrap_form', '~> 4.2'
+gem 'nokogiri', '~> 1.10', '>= 1.10.4'
 gem 'escape_utils', '~> 1.1'
 
 # Calendar rendering
 gem 'icalendar'
 
 # Diffs
-gem 'diffy', '~> 3.1.0'
+gem 'diffy', '~> 3.1'
 
 # Application server
-gem 'rack', '~> 2.0.7'
+gem 'rack', '~> 2.0', '>= 2.0.7'
 
 group :unicorn do
-  gem 'unicorn', '~> 5.4.1'
+  gem 'unicorn', '~> 5.4', '>= 5.4.1'
   gem 'unicorn-worker-killer', '~> 0.4.4'
 end
 
@@ -181,13 +181,13 @@
 gem 'acts-as-taggable-on', '~> 6.0'
 
 # Background jobs
-gem 'sidekiq', '~> 5.2.7'
+gem 'sidekiq', '~> 5.2', '>= 5.2.7'
 gem 'sidekiq-cron', '~> 1.0'
-gem 'redis-namespace', '~> 1.6.0'
+gem 'redis-namespace', '~> 1.6'
 gem 'gitlab-sidekiq-fetcher', '0.5.2', require: 'sidekiq-reliable-fetch'
 
 # Cron Parser
-gem 'fugit', '~> 1.2.1'
+gem 'fugit', '~> 1.2', '>= 1.2.1'
 
 # HTTP requests
 gem 'httparty', '~> 0.16.4'
@@ -199,14 +199,14 @@
 gem 'ruby-progressbar'
 
 # GitLab settings
-gem 'settingslogic', '~> 2.0.9'
+gem 'settingslogic', '~> 2.0', '>= 2.0.9'
 
 # Linear-time regex library for untrusted regular expressions
-gem 're2', '~> 1.1.1'
+gem 're2', '~> 1.1', '>= 1.1.1'
 
 # Misc
 
-gem 'version_sorter', '~> 2.2.4'
+gem 'version_sorter', '~> 2.2', '>= 2.2.4'
 
 # Export Ruby Regex to Javascript
 gem 'js_regex', '~> 3.1'
@@ -219,13 +219,13 @@
 gem 'connection_pool', '~> 2.0'
 
 # Redis session store
-gem 'redis-rails', '~> 5.0.2'
+gem 'redis-rails', '~> 5.0', '>= 5.0.2'
 
 # Discord integration
 gem 'discordrb-webhooks-blackst0ne', '~> 3.3', require: false
 
 # HipChat integration
-gem 'hipchat', '~> 1.5.0'
+gem 'hipchat', '~> 1.5'
 
 # Jira integration
 gem 'jira-ruby', '~> 1.7'
@@ -235,7 +235,7 @@
 gem 'flowdock', '~> 0.7'
 
 # Slack integration
-gem 'slack-notifier', '~> 1.5.1'
+gem 'slack-notifier', '~> 1.5', '>= 1.5.1'
 
 # Hangouts Chat integration
 gem 'hangouts-chat', '~> 0.0.5'
@@ -247,11 +247,11 @@
 gem 'ruby-fogbugz', '~> 0.2.1'
 
 # Kubernetes integration
-gem 'kubeclient', '~> 4.4.0'
+gem 'kubeclient', '~> 4.4'
 
 # Sanitize user input
 gem 'sanitize', '~> 4.6'
-gem 'babosa', '~> 1.0.2'
+gem 'babosa', '~> 1.0', '>= 1.0.2'
 
 # Sanitizes SVG input
 gem 'loofah', '~> 2.2'
@@ -260,10 +260,10 @@
 gem 'licensee', '~> 8.9'
 
 # Protect against bruteforcing
-gem 'rack-attack', '~> 4.4.1'
+gem 'rack-attack', '~> 4.4', '>= 4.4.1'
 
 # Ace editor
-gem 'ace-rails-ap', '~> 4.1.0'
+gem 'ace-rails-ap', '~> 4.1'
 
 # Detect and convert string character encoding
 gem 'charlock_holmes', '~> 0.7.5'
@@ -281,10 +281,10 @@
 gem 'webpack-rails', '~> 0.9.10'
 gem 'rack-proxy', '~> 0.6.0'
 
-gem 'sassc-rails', '~> 2.1.0'
-gem 'uglifier', '~> 2.7.2'
+gem 'sassc-rails', '~> 2.1'
+gem 'uglifier', '~> 2.7', '>=2.7.2'
 
-gem 'addressable', '~> 2.5.2'
+gem 'addressable', '~> 2.5', '>=2.5.2'
 gem 'font-awesome-rails', '~> 4.7'
 gem 'gemojione', '~> 3.3'
 gem 'gon', '~> 6.2'
@@ -296,7 +296,7 @@
 # Sentry integration
 gem 'sentry-raven', '~> 2.9'
 
-gem 'premailer-rails', '~> 1.9.7'
+gem 'premailer-rails', '~> 1.9', '>=1.9.7'
 
 # LabKit: Tracing and Correlation
 gem 'gitlab-labkit', '~> 0.5'
@@ -304,11 +304,11 @@
 # I18n
 gem 'ruby_parser', '~> 3.8', require: false
 gem 'rails-i18n', '~> 5.1'
-gem 'gettext_i18n_rails', '~> 1.8.0'
+gem 'gettext_i18n_rails', '~> 1.8'
 gem 'gettext_i18n_rails_js', '~> 1.3'
-gem 'gettext', '~> 3.2.2', require: false, group: :development
+gem 'gettext', '~> 3.2', '>= 3.2.2', require: false, group: :development
 
-gem 'batch-loader', '~> 1.4.0'
+gem 'batch-loader', '~> 1.4'
 
 # Perf bar
 # https://gitlab.com/gitlab-org/gitlab/issues/13996
@@ -426,11 +426,11 @@
 gem 'oauth2', '~> 1.4'
 
 # Health check
-gem 'health_check', '~> 2.6.0'
+gem 'health_check', '~> 2.6'
 
 # System information
-gem 'vmstat', '~> 2.3.0'
-gem 'sys-filesystem', '~> 1.1.6'
+gem 'vmstat', '~> 2.3'
+gem 'sys-filesystem', '~> 1.1', '>= 1.1.6'
 
 # NTP client
 gem 'net-ntp'
@@ -446,13 +446,13 @@
 end
 
 # Gitaly GRPC protocol definitions
-gem 'gitaly', '~> 1.65.0'
+gem 'gitaly', '~> 1.65'
 
-gem 'grpc', '~> 1.19.0'
+gem 'grpc', '~> 1.19'
 
-gem 'google-protobuf', '~> 3.7.1'
+gem 'google-protobuf', '~> 3.7', '>= 3.7.1'
 
-gem 'toml-rb', '~> 1.0.0', require: false
+gem 'toml-rb', '~> 1.0', require: false
 
 # Feature toggles
 gem 'flipper', '~> 0.13.0'
