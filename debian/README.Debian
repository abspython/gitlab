Debian specific changes in gitlab
=================================

1. Redis connection: redis-server package in debian follows upstream default
and listens on tcp port 6379. So gitlab package in debian is configured to use
tcp sockets. gitlab developers recommend using unix sockets. You can change to
using unix sockets by changing the following configuration files.

 /etc/redis/redis.conf and /etc/gitlab/resque.yml

2. wiki backend: debian package uses gollum-rugged_adapter whereas gitlab
upstream still use gollum-grit_adapter. grit is no longer maintained and grit
developers recommend switching to rugged. gollum-lib developers have announced
their intention to switch to rugged_adapter by default and this is in progress.

3. default paths: debian package has changed some default values for paths
which you can see at /etc/gitlab/gitlab-debian.conf

4. database: gitlab package configures postgresql database with peer
authentication.

5. gem versions: some gem dependency requirements are relaxed to work with
their packaged version in debian.

You can find the list of gems required by gitlab and their corresponding
package versions in debian at
http://debian.fosscommunity.in/status/?appname=gitlab&sort=satisfied

6. vendored js files: some embedded javascript files in
vendor/assets/javascripts are replaced by their packaged version.

7. root directory of rails is read only (/usr/share/gitlab); following symbolic
links are added to enable write access to gitlab app

> config -> /etc/gitlab
> Gemfile.lock -> /var/lib/gitlab/Gemfile.lock
> log -> /var/log/gitlab
> builds -> /var/log/gitlab/builds
> tmp -> /run/gitlab
> /run/gitlab/cache -> var/lib/gitlab/cache
> public -> /var/lib/gitlab/public
> shared -> /var/lib/gitlab/shared
> db -> /var/lib/gitlab/db
> /usr/share/gitlab/.secret -> /var/lib/gitlab/.secret

8. ssl certificates: This package tries to use letsencrypt package to obtain
ssl certificates, if it is installed (via Recommends). If letsencrypt is not
required, you can copy ssl certificate and key to /etc/gitlab/ssl as gitlab.crt
and gitlab.key. If letsencrypt option is selected, symbolic links are added for
certificates obtained using letsencrypt to /etc/gitlab/ssl.

9. exim compatibility issue: If you use exim as your mta, then see
https://github.com/gitlabhq/gitlabhq/issues/4866#issuecomment-145784636

Useful diagnostics
==================

Upstream documentation will instruct to run commands like the following:

    $ sudo -u gitlab -H bundle exec rake XXX RAILS_ENV=production

Where XXX is something like "db:migrate", "gitlab:check" or "gitlab:env:info".
In Debian, the rake command has to be called by the gitlab user from app home
directory /usr/share/gitlab and with the environment variables from
/etc/gitlab/gitlab-debian.conf set. So above command could be run like:

    $ runuser -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rake XXX RAILS_ENV=production'

One useful command to run in this environment is:

    $ bundle exec rake gitlab:check RAILS_ENV=production

Which will output helpful diagnostics about the state of your system including
how to fix possible problems. Another one is:

    $ bundle exec rake gitlab:env:info RAILS_ENV=production

To see service status with systemd, you can use:

    $ systemctl status gitlab.service -l
    $ systemctl status gitlab-unicorn.service -l
    $ systemctl status gitlab-sidekiq.service -l
    $ systemctl status gitlab-workhorse.service -l
    $ journalctl -xn

It is advised to attach the output of above commands to bugreports.

Migrating from non-Debian gitlab
================================

 0. Backup everything you don't want to loose like:
      - the postgresql database used by your gitlab instance
      - the repositories/ directory
      - the public/uploads/ directory
      - your .ssh/authorized_keys
 1. Remove the init script for your old gitlab installation, like:
      $ rm /etc/init.d/gitlab
 2. Install Debian gitlab:
      $ apt-get install gitlab
 3. Stop gitlab services:
      $ systemctl stop gitlab.service
 4. Rename your old database to gitlab_production and set the user gitlab as
    its owner and the owner of all its tables, sequences and views
      $ su - postgres
      $ psql
      # drop database gitlab_production;
      # alter database gitlabhq_production rename to gitlab_production;
      # alter database gitlab_production owner to gitlab;
      # \q
      $ for tbl in `psql -qAt -c "select tablename from pg_tables where schemaname = 'public';" gitlab_production` ; do  psql -c "alter table \"$tbl\" owner to gitlab" gitlab_production ; done
      $ for tbl in `psql -qAt -c "select sequence_name from information_schema.sequences where sequence_schema = 'public';" gitlab_production` ; do  psql -c "alter table \"$tbl\" owner to gitlab" gitlab_production ; done
 5. Copy your old repository directory to /var/lib/gitlab/repositories/
 6. Copy your old public/uploads/ directory to /var/lib/gitlab/public/uploads/
 7. Copy your old .ssh/authorized_keys to /var/lib/gitlab/.ssh/authorized_keys
 8. Fix your /var/lib/gitlab/.ssh/authorized_keys to contain the right path to gitlab-shell like:
      $ sed -i 's/^command="[^ ]\+gitlab-shell /command="\/usr\/share\/gitlab-shell\/bin\/gitlab-shell /' /usr/share/gitlab/.ssh/authorized_keys
 9. Fix permission:
      $ chown -R gitlab:gitlab /var/lib/gitlab/repositories/ /var/lib/gitlab/public/uploads/ /var/lib/gitlab/.ssh/authorized_keys
      $ chmod -R ug+rwX,o-rwx /var/lib/gitlab/repositories/
      $ find /var/lib/gitlab/public/uploads -type f -exec chmod 0644 {} \;
      $ find /var/lib/gitlab/public/uploads -type d -not -path /var/lib/gitlab/public/uploads -exec chmod 0700 {} \;
 10. Migrate the database:
      $ runuser -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rake db:migrate RAILS_ENV=production'
 11. Fix hooks:
      # su gitlab
      $ /usr/share/gitlab-shell/bin/create-hooks
 12. Start gitlab:
       $ systemctl start gitlab.service
 13. Check the installation:
      $ runuser -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rake gitlab:check RAILS_ENV=production'

Resetting admin password without web interface
==============================================
The steps involve dropping into rails console as gitlab user for production environment and then resetting the admin password via the user object.
    $ runuser -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rails console production'
    irb(main):001:0> user = User.where(admin: true).first
    irb(main):002:0> user.password = 'secret_pass'
    irb(main):003:0> user.password_confirmation = 'secret_pass'
    irb(main):004:0> user.save!

Granting an existing user admin access
======================================
The steps involve dropping into rails console as gitlab user for production environment and running the following commands.
    $ runuser -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rails console production'
    irb(main):001:0> user = User.find_by(email: 'useraddress@domain')
    irb(main):003:0> user.admin=true
    irb(main):004:0> user.save

Latest information regarding supported versions
===============================================
Subscribe to https://wiki.debian.org/gitlab to get updates about new package
versions in unofficial repositories.
